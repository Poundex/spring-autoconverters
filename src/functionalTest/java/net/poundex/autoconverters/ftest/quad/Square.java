package net.poundex.autoconverters.ftest.quad;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.List;


@Getter
@EqualsAndHashCode(callSuper = true)
@ToString
public class Square extends Quad {
    public Square(String id, int width, List<String> edgeIds) {
        super(id, width, edgeIds);
    }
}
