package net.poundex.autoconverters.ftest

import net.poundex.autoconverters.ftest.quad.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.convert.ConversionService
import spock.lang.Specification

@SpringBootTest(classes = FunctionalTestApplication)
class ConversionSpec extends Specification {

    @Autowired
    QuadApiConverters apiConverters

    @Autowired
    ConversionService conversionService

    void "Converter Component converts as expected"() {
        expect:
        apiConverters

        when: "Doing simple conversions"
        SquareDto squareDto = apiConverters.toDto(new Square("sqid", 10, null))
        RectangleDto rectangleDto = apiConverters.toDto(new Rectangle("rectid", 8, 7, null, null))

        then:
        squareDto.id() == "sqid"
        squareDto.width() == 10
        rectangleDto.id() == "rectid"
        rectangleDto.width() == 8
        rectangleDto.height() == 7

        when: "Doing abstract conversions"
        QuadDto squareDto2 = apiConverters.toDto((Quad) new Square("sq2id", 10, null))
        QuadDto rectangleDto2 = apiConverters.toDto((Quad) new Rectangle("rect2id", 8, 7, null, null))

        then:
        squareDto2 instanceof SquareDto
        rectangleDto2 instanceof RectangleDto

        then:
        squareDto2.id() == "sq2id"
        squareDto2.width() == 10
        rectangleDto2.id() == "rect2id"
        rectangleDto2.width() == 8
        rectangleDto2.height() == 7
    }

    void "Using ConversionService works as expected"() {
        when: "Doing simple conversions"
        SquareDto squareDto = conversionService.convert(new Square("sqid", 10, null), SquareDto)
        RectangleDto rectangleDto = conversionService.convert(new Rectangle("rectid", 8, 7, null, null), RectangleDto)

        then:
        squareDto.width() == 10
        rectangleDto.width() == 8
        rectangleDto.height() == 7

        when: "Doing abstract conversions"
        QuadDto squareDto2 = conversionService.convert((Quad) new Square("sq2id", 10, null), QuadDto)
        QuadDto rectangleDto2 = conversionService.convert((Quad) new Rectangle("rect2id", 8, 7, null, null), QuadDto)

        then:
        squareDto2 instanceof SquareDto
        rectangleDto2 instanceof RectangleDto

        then:
        squareDto2.width() == 10
        rectangleDto2.width() == 8
        rectangleDto2.height() == 7
    }
    
    void "Converts collections"() {
        when: "Doing simple conversions"
        SquareDto squareDto = apiConverters.toDto(new Square("sqid", 10, ["0", "1", "2", "3"]))

        then:
        squareDto.edgeIds() == [0, 1, 2, 3]
    }
}