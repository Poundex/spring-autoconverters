package net.poundex.autoconverters.ftest.quad

import net.poundex.autoconverters.ftest.FunctionalTestApplication
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(classes = FunctionalTestApplication)
class IgnorePropertiesSpec extends Specification {

    @Autowired
    QuadPersistentConverters converters

    void "Ignores ignored properties"() {
        given:
        ObjectId id = new ObjectId()
        ObjectId otherId = new ObjectId()
        Square square = new Square(otherId.toString(), 10, null)
        PersistentSquare persistentSquare = PersistentSquare.builder().id(id).build()
        
        PersistentRectangle persistentRectangle = PersistentRectangle.builder()
                .id(id)
                .width(10)
                .height(5)
                .ignoredProperty("ignore-this")
                .build()
        
        when:
        converters.updateFrom(square, persistentSquare)
        
        then:
        persistentSquare.id == id
        persistentSquare.width == 10
        
        when:
        Rectangle rect = converters.toDomain(persistentRectangle)
        
        then:
        rect.id == id.toString()
        rect.width == 10
        rect.height == 5
        rect.ignoredProperty == null
    }
}