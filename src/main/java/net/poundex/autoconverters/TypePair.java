package net.poundex.autoconverters;

import org.springframework.core.convert.TypeDescriptor;

import java.lang.reflect.Method;

public record TypePair(TypeDescriptor source, TypeDescriptor target) {
    
    public static TypePair fromClasses(Class<?> source, Class<?> target) {
        return new TypePair(TypeDescriptor.valueOf(source), TypeDescriptor.valueOf(target));
    }
    
    public static TypePair fromMethod(Method method) {
        return new TypePair(
                TypeDescriptor.valueOf(method.getParameterTypes()[0]), 
                TypeDescriptor.valueOf(method.getReturnType()));
    }
}
