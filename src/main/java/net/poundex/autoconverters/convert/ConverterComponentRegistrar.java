package net.poundex.autoconverters.convert;

import lombok.extern.slf4j.Slf4j;
import net.poundex.autoconverters.ReflectionHelper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.StandardAnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.util.StringUtils;

import java.util.function.Supplier;

@Slf4j
class ConverterComponentRegistrar implements ImportBeanDefinitionRegistrar, BeanFactoryAware, ReflectionHelper {

    private Supplier<ConfigurableConversionService> conversionService;
    private ConverterComponentFactory converterComponentFactory;
    
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.conversionService = beanFactory.getBeanProvider(ConfigurableConversionService.class)::getObject;
    }

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

       String basePackage;
       String explicitBp = importingClassMetadata
                .getAnnotationAttributes(EnableAutoConverters.class.getName())
                .get("basePackage")
                .toString();
       
        if(StringUtils.hasText(explicitBp))
            basePackage = explicitBp;
        else if(importingClassMetadata instanceof StandardAnnotationMetadata amd)
           basePackage = amd.getIntrospectedClass()
                .getPackage()
                .getName();
        else
            basePackage = classForName(importingClassMetadata.getClassName(), ((ConfigurableBeanFactory) registry).getBeanClassLoader()).getPackage().getName();

        this.converterComponentFactory = new ConverterComponentFactory(conversionService);

        ClassPathScanningCandidateComponentProvider p = new ClassPathScanningCandidateComponentProvider(false) {
            @Override
            protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
                return super.isCandidateComponent(beanDefinition)
                        || beanDefinition.getMetadata().isInterface();
            }
        };
        p.addIncludeFilter(new AnnotationTypeFilter(Converters.class, false, true));
        p.findCandidateComponents(basePackage).stream()
                .peek(def -> log.atInfo()
                        .setMessage("Creating Converters Component for discovered class")
                        .addKeyValue("class", def.getBeanClassName())
                        .log())
                .forEach(def -> converterComponentFactory
                        .createAndRegisterComponent(def.getBeanClassName(), registry));
    }
}
