package net.poundex.autoconverters.convert;

import lombok.extern.slf4j.Slf4j;
import net.poundex.autoconverters.ReflectionHelper;
import org.springframework.core.MethodParameter;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Slf4j
class ConstructorTargetWriter extends TargetWriter implements ReflectionHelper {

    private final Constructor<?> targetConstructor;

    public ConstructorTargetWriter(SourceReader sourceReader, Class<?> targetType, ConversionService conversionService, Set<String> ignoredProperties) {
        super(sourceReader, targetType, conversionService, ignoredProperties);
        this.targetConstructor = findConstructor(targetType);
    }

    Object write(Object source) {
        Object[] ctorArgsInOrder = Arrays.stream(targetConstructor.getParameters())
                .map(param -> getTargetValue(source, param))
                .toArray();
        return invokeConstructor(targetConstructor, ctorArgsInOrder);
    }

    private Object getTargetValue(Object source, Parameter param) {
        if(ignoredProperties.contains(param.getName()))
            return getDefaultValue(param.getType());
        
        return sourceReader.readProperty(source, param.getName())
                .map(sourceValue -> toWantedParamType(sourceValue,
                        new TypeDescriptor(MethodParameter.forParameter(param))))
                .orElseGet(() -> getDefaultValue(param.getType()));
    }

    private static Constructor<?> findConstructor(Class<?> targetType) {
        List<Constructor<?>> candidateTargetCtors = Arrays.stream(targetType.getConstructors())
                .filter(it -> it.getParameterCount() != 0)
                .toList();

        if (candidateTargetCtors.isEmpty())
            throw new IllegalStateException(
                    String.format("Did not find a non-default constructor in %s", targetType.getName()));

        if (candidateTargetCtors.size() != 1)
            throw new IllegalStateException(
                    String.format("Found more than one non-default constructor in %s", targetType.getName()));

        return candidateTargetCtors.get(0);
    }
}