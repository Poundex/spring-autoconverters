package net.poundex.autoconverters.convert;

import io.vavr.Lazy;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;
import lombok.RequiredArgsConstructor;
import net.poundex.autoconverters.ReflectionHelper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.core.convert.support.ConfigurableConversionService;

import java.lang.reflect.Method;
import java.util.function.Supplier;

@RequiredArgsConstructor
class ConverterComponentFactory implements ReflectionHelper {
    
    private final Supplier<ConfigurableConversionService> conversionServiceProvider;
    
    @SuppressWarnings("unchecked")
    <T> Supplier<T> createAndRegisterComponent(String mapperClassName, BeanDefinitionRegistry registry) {
        Class<T> mapperClass = (Class<T>) classForName(mapperClassName, ((ConfigurableBeanFactory) registry).getBeanClassLoader());
        Supplier<T> instanceSupplier = createComponent(mapperClass);
        registry.registerBeanDefinition("autoconverters" + mapperClass.getSimpleName(),
                BeanDefinitionBuilder.rootBeanDefinition(
                                mapperClass,
                                instanceSupplier)
                        .getBeanDefinition());
        return instanceSupplier;
    }
    
    @SuppressWarnings("unchecked")
    <T> Supplier<T> createComponent(Class<T> mapperClass) {
	    return () -> (T) createProxyInstance(mapperClass, createProxyClass(mapperClass));
    }

    private Class<?> createProxyClass(Class<?> mapperClass) {
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setInterfaces(new Class[]{ mapperClass });
        return proxyFactory.createClass();
    }
    
    private Object createProxyInstance(Class<?> mapperClass, Class<?> mapperImplClass) {
        ProxyObject mapperImpl = 
                (ProxyObject) invokeConstructor(mapperImplClass.getConstructors()[0]);
        mapperImpl.setHandler(createProxyHandler(mapperClass.getMethods()));
        return mapperImpl;
    }
    
    private ConverterComponentProxyHandler createProxyHandler(Method[] mapperMethods) {
        return new ConverterComponentProxyHandler(
                Lazy.val(conversionServiceProvider, ConfigurableConversionService.class), 
                mapperMethods);
    }
}
