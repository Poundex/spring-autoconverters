package net.poundex.autoconverters.convert;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import net.poundex.autoconverters.ReflectionHelper;
import org.springframework.core.ResolvableType;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.util.ReflectionUtils;

import java.beans.FeatureDescriptor;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
class SetterTargetWriter extends TargetWriter implements ReflectionHelper {

    private record Setter(String propertyName, TypeDescriptor propertyType, Method method) {}
    
    private final Map<String, Setter> setters;

    public SetterTargetWriter(SourceReader sourceReader, Class<?> targetType, ConversionService conversionService, Set<String> ignoredProperties) {
        super(sourceReader, targetType, conversionService, ignoredProperties);
        this.setters = Try.of(() -> Introspector.getBeanInfo(targetType))
                .toJavaStream()
                .flatMap(bi -> Arrays.stream(bi.getPropertyDescriptors()))
                .filter(prop -> ! ignoredProperties.contains(prop.getName()))
                .filter(prop -> prop.getWriteMethod() != null)
                .peek(prop -> ReflectionUtils.makeAccessible(prop.getWriteMethod()))
                .collect(Collectors.toMap(FeatureDescriptor::getName, this::toSetter));
    }

    void write(Object source, Object target) {
        setters.forEach((propName, setter) -> sourceReader.readProperty(source, propName)
                .ifPresent(v -> invokeMethod(setter.method(), target, toWantedParamType(v, setter.propertyType()))));
    }

    private Setter toSetter(PropertyDescriptor property) {
        return new Setter(property.getName(), new TypeDescriptor(
                ResolvableType.forMethodParameter(
                        property.getWriteMethod(), 0),
                null,
                null),
                property.getWriteMethod());
    }

}