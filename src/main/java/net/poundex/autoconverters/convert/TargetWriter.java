package net.poundex.autoconverters.convert;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.RequiredArgsConstructor;
import net.poundex.autoconverters.TypePair;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;

import java.lang.reflect.Array;
import java.util.Set;

@RequiredArgsConstructor
abstract class TargetWriter {
    
    protected static final LoadingCache<TypePair, Boolean> assignabilityCache = Caffeine.newBuilder()
            .build(types -> types.source().isAssignableTo(types.target()));
    
    protected final SourceReader sourceReader;
    protected final Class<?> targetType;
    protected final ConversionService conversionService;
    protected final Set<String> ignoredProperties;

    protected static Object getDefaultValue(Class<?> type) {
        if ( ! type.isPrimitive())
            return null;
        
        return Array.get(Array.newInstance(type, 1), 0);
    }

    protected Object toWantedParamType(SourceReader.TypedPropertyValue sourceValue, TypeDescriptor targetType) {
        if(sourceValue.value() == null)
            return null;
        
//        if(targetType.getType().isAssignableFrom(sourceValue.type().getType()) || 
//                sourceValue.type().isAssignableTo(targetType))
//            return sourceValue.value();
        
        if(assignabilityCache.get(new TypePair(sourceValue.type(), targetType)))
            return sourceValue.value();
        
        return conversionService.convert(sourceValue.value(), sourceValue.type(), targetType);
    }
}
