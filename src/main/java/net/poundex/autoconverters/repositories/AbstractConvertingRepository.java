package net.poundex.autoconverters.repositories;

import io.vavr.collection.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

@RequiredArgsConstructor
public class AbstractConvertingRepository<
		D,
		Dbo,
		Dao extends Repository<Dbo, ?>,
		Repo extends Repository<D, ?>> {
	
	protected final Dao dao;
	protected final ConversionService conversionService;
	
	
	@SuppressWarnings("unchecked")
	public <T> T adaptArguments(
			Object[] invokeArgs,
			BiFunction<Object, Object[], Object> targetExecutable, 
			Function<Object, Object> returnTypeAdapter, 
			List<Function<Object, Object>> paramsAdapters) {
	
		return (T) returnTypeAdapter.apply(targetExecutable.apply(dao,
				Stream.of(invokeArgs)
						.zip(paramsAdapters)
						.map(argAdapt -> argAdapt._2().apply(argAdapt._1()))
						.toJavaArray(Object[]::new)));
	}
}
