package net.poundex.autoconverters;

import org.springframework.util.ReflectionUtils;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public interface ReflectionHelper {
    default Object invokeConstructor(Constructor<?> constructor, Object... args) {
        try {
            ReflectionUtils.makeAccessible(constructor);
            return constructor.newInstance(args);
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    default Class<?> classForName(String name, ClassLoader classLoader) {
        try {
            return Class.forName(name, true, classLoader);
        } catch (ClassNotFoundException cnfex) {
            throw new RuntimeException(cnfex);
        }
    }
    
    default Object invokeMethod(Method method, Object target, Object... args) {
        try {
            return method.invoke(target, args);
        } catch (IllegalAccessException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    default Object invokeMethodHandle(MethodHandle mh, Object target) {
        try {
            return mh.invoke(target);
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        }
    }
    
    default MethodHandle unreflect(MethodHandles.Lookup lookup, Method method) {
        try {
            return lookup.unreflect(method);
        } catch (IllegalAccessException iaex) {
            throw new RuntimeException(iaex);
        }
    }
    
}
