package net.poundex.autoconverters.support;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.core.convert.support.ConfigurableConversionService;
import reactor.core.publisher.Mono;

@AutoConfiguration
@RequiredArgsConstructor
public class ConverterConfiguration {
	
	private final ConfigurableConversionService conversionService;
	
	@PostConstruct
	void init() {
		conversionService.addConverter(new FluxConverter(conversionService));
		conversionService.addConverter(new MonoConverter(conversionService));
	}
}
