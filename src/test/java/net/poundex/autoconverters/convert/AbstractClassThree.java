package net.poundex.autoconverters.convert;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public abstract class AbstractClassThree {
    private final String simpleString;
}
