package net.poundex.autoconverters.convert;

public record ConversionTarget(String string, String integer) {
}
