package net.poundex.autoconverters.convert;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ClassTwo {
    private final String simpleString;
    private final int skip;
}
