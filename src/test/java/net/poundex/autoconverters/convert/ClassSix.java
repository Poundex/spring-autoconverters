package net.poundex.autoconverters.convert;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ClassSix implements ClassFiveOrSix {
    private final String simpleString;
}
