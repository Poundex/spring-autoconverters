package net.poundex.autoconverters.convert;

public record RecordTwo(String simpleString, int skip) {
}
