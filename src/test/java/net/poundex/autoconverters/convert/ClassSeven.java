package net.poundex.autoconverters.convert;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class ClassSeven {
    private final int number;
    private final String string;
    private final int stringFromNumber;
}
