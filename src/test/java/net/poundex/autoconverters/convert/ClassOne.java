package net.poundex.autoconverters.convert;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class ClassOne {
    private final String simpleString;
}
