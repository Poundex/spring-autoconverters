package net.poundex.autoconverters.convert;

public record ConversionSource(String string, Integer integer) {
}
