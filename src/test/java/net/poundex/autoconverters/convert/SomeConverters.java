package net.poundex.autoconverters.convert;

@Converters
public interface SomeConverters {
    ClassTwo toClassTwo(ClassOne classOne);
    
    @Abstract
    ClassFiveOrSix toFiveOrSix(AbstractClassThree abstractClassThree);
    ClassFive toFive(ClassThreeOne classThreeOne);
    ClassSix toSix(ClassThreeTwo classThreeTwo);

    @IgnoreTargetProperty("id")
    void copyToMutable(ClassSeven classSeven, MutableClass mutableClass);
}
