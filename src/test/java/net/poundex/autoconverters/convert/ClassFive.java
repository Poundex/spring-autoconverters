package net.poundex.autoconverters.convert;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class ClassFive implements ClassFiveOrSix {
    private final String simpleString;
}
