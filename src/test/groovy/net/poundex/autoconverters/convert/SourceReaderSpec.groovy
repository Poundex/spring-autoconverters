package net.poundex.autoconverters.convert

import net.poundex.autoconverters.convert.ClassOne
import net.poundex.autoconverters.convert.RecordOne
import net.poundex.autoconverters.convert.SourceReader
import org.springframework.core.ResolvableType
import spock.lang.Specification
import spock.lang.Subject

class SourceReaderSpec extends Specification {
    
    void "Returns empty when reading non-existent bean property"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(ClassOne)
        ClassOne source = new ClassOne("some-string")
        
        when:
        Optional<Object> res = sourceReader.readProperty(source, "noSuchProperty")
        
        then:
        res.isEmpty()
    }

    void "Returns value when reading bean property"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(ClassOne)
        ClassOne source = new ClassOne("some-string")

        when:
        Optional<SourceReader.TypedPropertyValue> res = sourceReader.readProperty(source, "simpleString")

        then:
        res.get().type().type == ResolvableType.forClass(String).type
        res.get().value() == "some-string"
    }
    
    void "Returns empty when reading non-existent record component"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(RecordOne)
        RecordOne source = new RecordOne("some-string")

        when:
        Optional<Object> res = sourceReader.readProperty(source, "noSuchProperty")

        then:
        res.isEmpty()
    }

    void "Returns value when reading record component"() {
        given:
        @Subject SourceReader sourceReader = SourceReader.forType(RecordOne)
        RecordOne source = new RecordOne("some-string")

        when:
        Optional<SourceReader.TypedPropertyValue> res = sourceReader.readProperty(source, "simpleString")

        then:
        res.get().type().type == ResolvableType.forClass(String).type
        res.get().value() == "some-string"
    }
}
