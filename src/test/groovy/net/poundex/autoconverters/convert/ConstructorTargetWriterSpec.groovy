package net.poundex.autoconverters.convert

import net.poundex.autoconverters.convert.ClassOne
import net.poundex.autoconverters.convert.ClassTwo
import net.poundex.autoconverters.convert.ConstructorTargetWriter
import net.poundex.autoconverters.convert.ConversionSource
import net.poundex.autoconverters.convert.ConversionTarget
import net.poundex.autoconverters.convert.RecordOne
import net.poundex.autoconverters.convert.RecordTwo
import net.poundex.autoconverters.convert.SourceReader
import org.springframework.core.convert.ConversionService
import org.springframework.core.convert.TypeDescriptor
import spock.lang.Specification
import spock.lang.Subject

class ConstructorTargetWriterSpec extends Specification {
    
    ConversionService conversionService = Mock()
    
    void "Writes bean targets"() {
        given:
        @Subject ConstructorTargetWriter writer = 
                new ConstructorTargetWriter(SourceReader.forType(ClassOne), ClassTwo, Stub(ConversionService), [].toSet())

        when:
        def res = writer.write(new ClassOne("some-string"))
        
        then:
        res instanceof ClassTwo
        res.simpleString == "some-string"
    }
    
    void "Writes record targets"() {
        given:
        @Subject ConstructorTargetWriter writer =
                new ConstructorTargetWriter(SourceReader.forType(RecordOne), RecordTwo, Stub(ConversionService), [].toSet())

        when:
        def res = writer.write(new RecordOne("some-string"))

        then:
        res instanceof RecordTwo
        res.simpleString() == "some-string"
    }
    
    void "Converts types when required"() {
        given:
        @Subject ConstructorTargetWriter writer =
                new ConstructorTargetWriter(SourceReader.forType(ConversionSource), ConversionTarget, conversionService, [].toSet())

        when:
        def res = writer.write(new ConversionSource("some-string", 123))

        then:
        1 * conversionService.convert(123, TypeDescriptor.valueOf(Integer), TypeDescriptor.valueOf(String)) >> "123"
        0 * conversionService._
        
        and:
        res instanceof ConversionTarget
        res.string() == "some-string"
        res.integer() instanceof String
        res.integer() == "123"
    }
    
    void "Ignores ignored properties"() {
        given:
        @Subject ConstructorTargetWriter writer =
                new ConstructorTargetWriter(SourceReader.forType(ClassOne), ClassTwo, Stub(ConversionService), ["simpleString"].toSet())

        when:
        def res = writer.write(new ClassOne("some-string"))

        then:
        res instanceof ClassTwo
        res.simpleString == null
    }
}
