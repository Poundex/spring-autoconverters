package net.poundex.autoconverters.convert

import net.poundex.autoconverters.convert.ClassSeven
import net.poundex.autoconverters.convert.MutableClass
import net.poundex.autoconverters.convert.SetterTargetWriter
import net.poundex.autoconverters.convert.SourceReader
import org.springframework.core.convert.ConversionService
import spock.lang.Specification
import spock.lang.Subject

class SetterTargetWriterSpec extends Specification {
    
    ConversionService conversionService = Stub()
    
    void "Copies properties to target"() {
        given:
        @Subject SetterTargetWriter writer = new SetterTargetWriter(
		        SourceReader.forType(ClassSeven),
                MutableClass, 
                conversionService, 
                ["number"].toSet())


        MutableClass mutableTarget = new MutableClass()
        mutableTarget.setNumber(456)
        
        conversionService.convert(789, _, _) >> "789"
        
        when:
        writer.write(new ClassSeven(123, "some-string", 789), mutableTarget)

        then: "Same types are copied as-is"
        mutableTarget.string == "some-string"

        and: "Properties are converted when required"
        mutableTarget.stringFromNumber == "789"

        and: "Ignored properties are ignored"
        mutableTarget.number == 456
    }
}
